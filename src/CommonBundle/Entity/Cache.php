<?php

namespace MovingaCommon\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Cache
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="MovingaCommon\CommonBundle\Repository\CacheRepository")
 */
class Cache
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cacheKey", type="string", length=255)
     */
    private $cacheKey;

    /**
     * @var float
     *
     * @ORM\Column(name="value", type="float")
     */
    private $value;

    /**
     * @var string
     *
     * @ORM\Column(name="owner", type="string", length=127)
     */
    private $owner;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set cacheKey
     *
     * @param string $cacheKey
     *
     * @return Cache
     */
    public function setCacheKey($cacheKey)
    {
        $this->cacheKey = $cacheKey;

        return $this;
    }

    /**
     * Get cacheKey
     *
     * @return string
     */
    public function getCacheKey()
    {
        return $this->cacheKey;
    }

    /**
     * Set value
     *
     * @param float $value
     *
     * @return Cache
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value
     *
     * @return float
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set owner
     *
     * @param string $owner
     *
     * @return Cache
     */
    public function setOwner($owner)
    {
        $this->owner = $owner;

        return $this;
    }

    /**
     * Get owner
     *
     * @return string
     */
    public function getOwner()
    {
        return $this->owner;
    }
}

