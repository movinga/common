<?php

namespace MovingaCommon\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Comment
 *
 * @ORM\Table(name="Comment", indexes={@ORM\Index(name="order_id", columns={"order_id"})})
 * @ORM\Entity
 */
class Comment
{

    /**
     * @var string
     *
     * @ORM\Column(name="customer", type="text", length=65535, nullable=true)
     */
    private $customer;

    /**
     * @var string
     *
     * @ORM\Column(name="claim", type="text", length=65535, nullable=true)
     */
    private $claim;

    /**
     * @var string
     *
     * @ORM\Column(name="supplier", type="text", length=65535, nullable=true)
     */
    private $supplier;

    /**
     * @var string
     *
     * @ORM\Column(name="team", type="text", length=65535, nullable=true)
     */
    private $team;

    /**
     * @var string
     *
     * @ORM\Column(name="parking", type="text", length=65535, nullable=true)
     */
    private $parking;

    /**
     * @var string
     *
     * @ORM\Column(name="sales", type="text", length=65535, nullable=true)
     */
    private $sales;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var Order
     *
     * @ORM\OneToOne(targetEntity="Order", inversedBy="comment")
     *
     */
    private $order;

    /**
     * Set customer
     *
     * @param string $customer
     *
     * @return Comment
     */
    public function setCustomer($customer)
    {
        $this->customer = $customer;

        return $this;
    }

    /**
     * Get customer
     *
     * @return string
     */
    public function getCustomer()
    {
        return $this->customer;
    }

    /**
     * Set claim
     *
     * @param string $claim
     *
     * @return Comment
     */
    public function setClaim($claim)
    {
        $this->claim = $claim;

        return $this;
    }

    /**
     * Get claim
     *
     * @return string
     */
    public function getClaim()
    {
        return $this->claim;
    }

    /**
     * Set supplier
     *
     * @param string $supplier
     *
     * @return Comment
     */
    public function setSupplier($supplier)
    {
        $this->supplier = $supplier;

        return $this;
    }

    /**
     * Get supplier
     *
     * @return string
     */
    public function getSupplier()
    {
        return $this->supplier;
    }

    /**
     * Set team
     *
     * @param string $team
     *
     * @return Comment
     */
    public function setTeam($team)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return string
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set order
     *
     * @param Order $order
     *
     * @return Comment
     */
    public function setOrder(Order $order = null)
    {
        $this->order = $order;

        return $this;
    }

    /**
     * Get order
     *
     * @return Order
     */
    public function getOrder()
    {
        return $this->order;
    }

    /**
     * Set parking
     *
     * @param string $parking
     *
     * @return Comment
     */
    public function setParking($parking)
    {
        $this->parking = $parking;

        return $this;
    }

    /**
     * Get parking
     *
     * @return string
     */
    public function getParking()
    {
        return $this->parking;
    }

    /**
     * Set sales
     *
     * @param string $sales
     *
     * @return Comment
     */
    public function setSales($sales)
    {
        $this->sales = $sales;

        return $this;
    }

    /**
     * Get sales
     *
     * @return string
     */
    public function getSales()
    {
        return $this->sales;
    }
}
