<?php

namespace MovingaCommon\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Order
 *
 * @ORM\Table(
 *  name="`Order`",
 *  indexes={
 *      @ORM\Index(
 *          name="lead_id_idx",
 *          columns={"provider_id", "lead_id"}
 *      ),
 *      @ORM\Index(
 *          name="salesforce_opportunity_id_idx",
 *          columns={"salesforce_opportunity_id"}
 *      ),
 *      @ORM\Index(
 *          name="salesforce_lead_id_idx",
 *          columns={"salesforce_lead_id"}
 *      ),
 *      @ORM\Index(
 *          name="salesforce_account_id_idx",
 *          columns={"salesforce_account_id"}
 *      )})
 * @ORM\Entity(repositoryClass="MovingaCommon\CommonBundle\Repository\OrderRepository")
 */
class Order implements \JsonSerializable
{
    const GOOGLE_API_URL = 'https://maps.googleapis.com/maps/api/distancematrix/json?';
    const TYPE_FROM = 0;
    const TYPE_TO = 1;
    const BILLING_ADDRESS_NOT_SUPPLIED = 0;
    const BILLING_ADDRESS_MOVE_OUT = 1;
    const BILLING_ADDRESS_MOVE_IN = 2;
    const BILLING_ADDRESS_OTHER = 3;

    /**
     * @var string
     *
     * @ORM\Column(name="lead_id", type="string", length=50, nullable=true)
     */
    private $leadId;

    /**
     * @var string
     *
     * @ORM\Column(name="lead_source", type="string", length=255, nullable=true)
     */
    private $leadSource;

    /**
     * @var string
     *
     * @ORM\Column(name="provider_id", type="string", length=50, nullable=true)
     */
    private $providerId;

    /**
     * @var string
     *
     * @ORM\Column(name="salesforce_opportunity_id", type="string", length=50, nullable=true)
     */
    private $salesforceOpportunityId;

    /**
     * @var string
     *
     * @ORM\Column(name="salesforce_lead_id", type="string", length=50, nullable=true)
     */
    private $salesforceLeadId;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="lead_received", type="datetime", nullable=true)
     */
    private $leadReceived;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="offer_sent", type="datetime", nullable=true)
     */
    private $offerSent;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="offer_received", type="datetime", nullable=true)
     */
    private $offerReceived;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_moving", type="datetime", nullable=true)
     */
    private $dateMoving;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_closed", type="datetime", nullable=true)
     */
    private $dateClosed;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_moving_alt", type="datetime", nullable=true)
     */
    private $dateMovingAlt;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date_arriving", type="datetime", nullable=true)
     */
    private $dateArriving;

    /**
     * @var boolean
     *
     * @ORM\Column(name="date_flexible", type="boolean", nullable=true)
     */
    private $dateFlexible;

    /**
     * @var string
     *
     * @ORM\Column(name="payment", type="string", length=50, nullable=true)
     */
    private $payment;

    /**
     * @var boolean
     *
     * @ORM\Column(name="payment_status", type="boolean", nullable=true)
     */
    private $paymentStatus;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean", nullable=true)
     */
    private $status;

    /**
     * @var string
     *
     * @ORM\Column(name="stage_name", type="string", length=100, nullable=true)
     */
    private $stageName;

    /**
     * @var string
     *
     * @ORM\Column(name="transport_partner", type="string", length=100, nullable=true)
     */
    private $transportPartner;

    /**
     * @var string
     *
     * @ORM\Column(name="salesperson", type="string", length=100, nullable=true)
     */
    private $salesperson;

    /**
     * @var string
     *
     * @ORM\Column(name="opsperson", type="string", length=100, nullable=true)
     */
    private $opsperson;

    /**
     * @var string
     *
     * @ORM\Column(name="financeperson", type="string", length=100, nullable=true)
     */
    private $financeperson;

    /**
     * @var string
     *
     * @ORM\Column(name="customersperson", type="string", length=100, nullable=true)
     */
    private $customersperson;

    /**
     * @var float
     *
     * @ORM\Column(name="initialprice", type="decimal", precision=10, scale=2, nullable=true)
     */
    private $initialprice;

    /**
     * @var float
     *
     * @ORM\Column(name="realizedprice", type="float", precision=10, scale=0, nullable=true)
     */
    private $realizedprice;

    /**
     * @var float
     *
     * @ORM\Column(name="targetcost", type="float", precision=10, scale=0, nullable=true)
     */
    private $targetcost;

    /**
     * @var float
     *
     * @ORM\Column(name="realizedcost", type="float", precision=10, scale=0, nullable=true)
     */
    private $realizedcost;

    /**
     * @var float
     *
     * @ORM\Column(name="discount_abs", type="float", precision=10, scale=0, nullable=true)
     */
    private $discountAbs;

    /**
     * @var boolean
     *
     * @ORM\Column(name="discount_rel", type="boolean", nullable=true)
     */
    private $discountRel;

    /**
     * @var string
     *
     * @ORM\Column(name="currency", type="string", length=3, nullable=true)
     */
    private $currency;

    /**
     * @var string
     *
     * @ORM\Column(name="salesforce_account_id", type="string", length=50, nullable=true)
     */
    private $salesForceAccountId;

    /**
     * @var string
     *
     * @ORM\Column(name="company", type="string", length=100, nullable=true)
     */
    private $company;

    /**
     * @var string
     *
     * @ORM\Column(name="gender", type="string", length=30, nullable=true)
     */
    private $gender;

    /**
     * @var string
     *
     * @ORM\Column(name="firstname", type="string", length=100, nullable=true)
     */
    private $firstname;

    /**
     * @var string
     *
     * @ORM\Column(name="lastname", type="string", length=100, nullable=true)
     */
    private $lastname;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=100, nullable=true)
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="address2", type="string", length=100, nullable=true)
     */
    private $address2;

    /**
     * @var string
     *
     * @ORM\Column(name="zip", type="string", length=8, nullable=true)
     */
    private $zip;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=50, nullable=true)
     */
    private $city;

    /**
     * @var string
     *
     * @ORM\Column(name="state", type="string", length=50, nullable=true)
     */
    private $state;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=80, nullable=true)
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=true)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=50, nullable=true)
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="mobile", type="string", length=50, nullable=true)
     */
    private $mobile;

    /**
     * @var string
     *
     * @ORM\Column(name="language", type="string", length=50, nullable=true)
     */
    private $language;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="guid", type="string", length=50, nullable=true, unique=true)
     */
    private $guid;


    /**
     * @var integer
     *
     * @ORM\Column(name="transport_type", type="integer")
     * @ORM\ManyToOne(targetEntity="TransportType", inversedBy="id")
     */
    private $transportType = 1;

    /**
     * @var Comment
     *
     * @ORM\OneToOne(targetEntity="Comment", mappedBy="order", cascade={"persist", "remove"})
     */
    protected $comment;

    /**
     * @var Property2order
     *
     * @ORM\OneToMany(targetEntity="Property2order", mappedBy="order", cascade={"persist", "remove"})
     */
    protected $properties;

    /**
     * @var Address
     *
     * @ORM\OneToMany(targetEntity="Address", mappedBy="order", cascade={"persist", "remove"})
     * @ORM\OrderBy({"position"="ASC"})
     */
    protected $addresses;

    /**
     * @var bool
     *
     * @ORM\Column(name="exported", type="boolean")
     */
    protected $exported = false;

    /**
     * @var bool
     *
     * @ORM\Column(name="locked", type="boolean")
     */
    protected $locked = false;

    /**
     * @ORM\OneToOne(targetEntity="\PaymentBundle\Entity\Hash", inversedBy="order", cascade={"persist"})
     */
    protected $hash;

    /**
     * @var integer
     *
     * @ORM\Column(name="billing_address_type", type="integer")
     */
    private $billingAddressType = self::BILLING_ADDRESS_NOT_SUPPLIED;

    /**
     * @var integer
     *
     * @ORM\Column(name="customer_information", type="text", nullable=true)
     */
    private $customerInformation = '';

    /**
     * @var Insurance
     *
     * @ORM\OneToMany(targetEntity="\LeadsBundle\Entity\Insurance", mappedBy="order", cascade={"persist", "remove"})
     */
    private $insurances;

    /**
     * @return int
     */
    public function getCustomerInformation()
    {
        return $this->customerInformation;
    }

    /**
     * @param int $customerInformation
     */
    public function setCustomerInformation($customerInformation)
    {
        $this->customerInformation = $customerInformation;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->properties = new \Doctrine\Common\Collections\ArrayCollection();
        $this->addresses = new \Doctrine\Common\Collections\ArrayCollection();
        $this->insurances = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Set leadId
     *
     * @param string $leadId
     *
     * @return Order
     */
    public function setLeadId($leadId)
    {
        $this->leadId = $leadId;

        return $this;
    }

    /**
     * Get leadId
     *
     * @return string
     */
    public function getLeadId()
    {
        return $this->leadId;
    }

    /**
     * Set leadSource
     *
     * @param string $leadSource
     *
     * @return Order
     */
    public function setLeadSource($leadSource)
    {
        $this->leadSource = $leadSource;

        return $this;
    }

    /**
     * Get leadSource
     *
     * @return string
     */
    public function getLeadSource()
    {
        return $this->leadSource;
    }

    /**
     * Set providerId
     *
     * @param string $providerId
     *
     * @return Order
     */
    public function setProviderId($providerId)
    {
        $this->providerId = $providerId;

        return $this;
    }

    /**
     * Get providerId
     *
     * @return string
     */
    public function getProviderId()
    {
        return $this->providerId;
    }

    /**
     * Set leadReceived
     *
     * @param \DateTime $leadReceived
     *
     * @return Order
     */
    public function setLeadReceived($leadReceived)
    {
        $this->leadReceived = $leadReceived;

        return $this;
    }

    /**
     * Get leadReceived
     *
     * @return \DateTime
     */
    public function getLeadReceived()
    {
        return $this->leadReceived;
    }

    /**
     * Set offerSent
     *
     * @param \DateTime $offerSent
     *
     * @return Order
     */
    public function setOfferSent($offerSent)
    {
        $this->offerSent = $offerSent;

        return $this;
    }

    /**
     * Get offerSent
     *
     * @return \DateTime
     */
    public function getOfferSent()
    {
        return $this->offerSent;
    }

    /**
     * Set offerReceived
     *
     * @param \DateTime $offerReceived
     *
     * @return Order
     */
    public function setOfferReceived($offerReceived)
    {
        $this->offerReceived = $offerReceived;

        return $this;
    }

    /**
     * Get offerReceived
     *
     * @return \DateTime
     */
    public function getOfferReceived()
    {
        return $this->offerReceived;
    }

    /**
     * Set dateMoving
     *
     * @param \DateTime $dateMoving
     *
     * @return Order
     */
    public function setDateMoving($dateMoving)
    {
        $this->dateMoving = $dateMoving;

        return $this;
    }

    /**
     * Get dateMoving
     *
     * @return \DateTime
     */
    public function getDateMoving()
    {
        return $this->dateMoving;
    }

    /**
     * Set dateClosed
     *
     * @param \DateTime $dateClosed
     *
     * @return Order
     */
    public function setDateClosed($dateClosed)
    {
        $this->dateClosed = $dateClosed;

        return $this;
    }

    /**
     * Get dateClosed
     *
     * @return \DateTime
     */
    public function getDateClosed()
    {
        return $this->dateClosed;
    }


    /**
     * Set dateMovingAlt
     *
     * @param \DateTime $dateMovingAlt
     *
     * @return Order
     */
    public function setDateMovingAlt($dateMovingAlt)
    {
        $this->dateMovingAlt = $dateMovingAlt;

        return $this;
    }

    /**
     * Get dateMovingAlt
     *
     * @return \DateTime
     */
    public function getDateMovingAlt()
    {
        return $this->dateMovingAlt;
    }

    /**
     * Set dateArriving
     *
     * @param \DateTime $dateArriving
     *
     * @return Order
     */
    public function setDateArriving($dateArriving)
    {
        $this->dateArriving = $dateArriving;

        return $this;
    }

    /**
     * Get dateArriving
     *
     * @return \DateTime
     */
    public function getDateArriving()
    {
        return $this->dateArriving;
    }

    /**
     * Set dateFlexible
     *
     * @param boolean $dateFlexible
     *
     * @return Order
     */
    public function setDateFlexible($dateFlexible)
    {
        $this->dateFlexible = $dateFlexible;

        return $this;
    }

    /**
     * Get dateFlexible
     *
     * @return boolean
     */
    public function getDateFlexible()
    {
        return $this->dateFlexible;
    }

    /**
     * Set payment
     *
     * @param string $payment
     *
     * @return Order
     */
    public function setPayment($payment)
    {
        $this->payment = $payment;

        return $this;
    }

    /**
     * Get payment
     *
     * @return string
     */
    public function getPayment()
    {
        return $this->payment;
    }

    /**
     * Set paymentStatus
     *
     * @param boolean $paymentStatus
     *
     * @return Order
     */
    public function setPaymentStatus($paymentStatus)
    {
        $this->paymentStatus = $paymentStatus;

        return $this;
    }

    /**
     * Get paymentStatus
     *
     * @return boolean
     */
    public function getPaymentStatus()
    {
        return $this->paymentStatus;
    }

    /**
     * Set status
     *
     * @param boolean $status
     *
     * @return Order
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return boolean
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set transportPartner
     *
     * @param string $transportPartner
     *
     * @return Order
     */
    public function setTransportPartner($transportPartner)
    {
        $this->transportPartner = $transportPartner;

        return $this;
    }

    /**
     * Get transportPartner
     *
     * @return string
     */
    public function getTransportPartner()
    {
        return $this->transportPartner;
    }

    /**
     * Set salesperson
     *
     * @param string $salesperson
     *
     * @return Order
     */
    public function setSalesperson($salesperson)
    {
        $this->salesperson = $salesperson;

        return $this;
    }

    /**
     * Get salesperson
     *
     * @return string
     */
    public function getSalesperson()
    {
        return $this->salesperson;
    }

    /**
     * Set opsperson
     *
     * @param string $opsperson
     *
     * @return Order
     */
    public function setOpsperson($opsperson)
    {
        $this->opsperson = $opsperson;

        return $this;
    }

    /**
     * Get opsperson
     *
     * @return string
     */
    public function getOpsperson()
    {
        return $this->opsperson;
    }

    /**
     * Set financeperson
     *
     * @param string $financeperson
     *
     * @return Order
     */
    public function setFinanceperson($financeperson)
    {
        $this->financeperson = $financeperson;

        return $this;
    }

    /**
     * Get financeperson
     *
     * @return string
     */
    public function getFinanceperson()
    {
        return $this->financeperson;
    }

    /**
     * Set customersperson
     *
     * @param string $customersperson
     *
     * @return Order
     */
    public function setCustomersperson($customersperson)
    {
        $this->customersperson = $customersperson;

        return $this;
    }

    /**
     * Get customersperson
     *
     * @return string
     */
    public function getCustomersperson()
    {
        return $this->customersperson;
    }

    /**
     * Set initialprice
     *
     * @param float $initialprice
     *
     * @return Order
     */
    public function setInitialprice($initialprice)
    {
        $this->initialprice = $initialprice;

        return $this;
    }

    /**
     * Get initialprice
     *
     * @return float
     */
    public function getInitialprice()
    {
        return $this->initialprice;
    }

    /**
     * Set realizedprice
     *
     * @param float $realizedprice
     *
     * @return Order
     */
    public function setRealizedprice($realizedprice)
    {
        $this->realizedprice = $realizedprice;

        return $this;
    }

    /**
     * Get realizedprice
     *
     * @return float
     */
    public function getRealizedprice()
    {
        return $this->realizedprice;
    }

    /**
     * Set targetcost
     *
     * @param float $targetcost
     *
     * @return Order
     */
    public function setTargetcost($targetcost)
    {
        $this->targetcost = $targetcost;

        return $this;
    }

    /**
     * Get targetcost
     *
     * @return float
     */
    public function getTargetcost()
    {
        return $this->targetcost;
    }

    /**
     * Set realizedcost
     *
     * @param float $realizedcost
     *
     * @return Order
     */
    public function setRealizedcost($realizedcost)
    {
        $this->realizedcost = $realizedcost;

        return $this;
    }

    /**
     * Get realizedcost
     *
     * @return float
     */
    public function getRealizedcost()
    {
        return $this->realizedcost;
    }

    /**
     * Set discountAbs
     *
     * @param float $discountAbs
     *
     * @return Order
     */
    public function setDiscountAbs($discountAbs)
    {
        $this->discountAbs = $discountAbs;

        return $this;
    }

    /**
     * Get discountAbs
     *
     * @return float
     */
    public function getDiscountAbs()
    {
        return $this->discountAbs;
    }

    /**
     * Set discountRel
     *
     * @param boolean $discountRel
     *
     * @return Order
     */
    public function setDiscountRel($discountRel)
    {
        $this->discountRel = $discountRel;

        return $this;
    }

    /**
     * Get discountRel
     *
     * @return boolean
     */
    public function getDiscountRel()
    {
        return $this->discountRel;
    }

    /**
     * Set currency
     *
     * @param string $currency
     *
     * @return Order
     */
    public function setCurrency($currency)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency
     *
     * @return string
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * Set company
     *
     * @param string $company
     *
     * @return Order
     */
    public function setCompany($company)
    {
        $this->company = $company;

        return $this;
    }

    /**
     * Get company
     *
     * @return string
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * Set gender
     *
     * @param string $gender
     *
     * @return Order
     */
    public function setGender($gender)
    {
        $this->gender = $gender;

        return $this;
    }

    /**
     * Get gender
     *
     * @return string
     */
    public function getGender()
    {
        return $this->gender;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     *
     * @return Order
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     *
     * @return Order
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Order
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set address2
     *
     * @param string $address2
     *
     * @return Order
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set zip
     *
     * @param string $zip
     *
     * @return Order
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set city
     *
     * @param string $city
     *
     * @return Order
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Order
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set country
     *
     * @param string $country
     *
     * @return Order
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Order
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Order
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set mobile
     *
     * @param string $mobile
     *
     * @return Order
     */
    public function setMobile($mobile)
    {
        $this->mobile = $mobile;

        return $this;
    }

    /**
     * Get mobile
     *
     * @return string
     */
    public function getMobile ()
    {
        return $this->mobile;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add property
     *
     * @param Property2order $property
     *
     * @return Order
     */
    public function addProperty(Property2order $property)
    {
        $this->properties[] = $property;

        return $this;
    }

    /**
     * Remove property
     *
     * @param Property2order $property
     */
    public function removeProperty(Property2order $property)
    {
        $this->properties->removeElement($property);
    }

    /**
     * Get properties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProperties()
    {
        return $this->properties;
    }

    /**
     * Add address
     *
     * @param Address $address
     *
     * @return Order
     */
    public function addAddress(Address $address)
    {
        $this->addresses[] = $address;

        return $this;
    }

    /**
     * Remove address
     *
     * @param Address $address
     */
    public function removeAddress(Address $address)
    {
        $this->addresses->removeElement($address);
    }

    /**
     * Get addresses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAddresses()
    {
        return $this->addresses;
    }

    /**
     * Set language
     *
     * @param string $language
     *
     * @return Order
     */
    public function setLanguage($language)
    {
        $this->language = $language;

        return $this;
    }

    /**
     * Get language
     *
     * @return string
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        unset($vars['leadReceived']);

        return $vars;
    }

    /**
     * Set comment
     *
     * @param Comment $comment
     *
     * @return Order
     */
    public function setComment(Comment $comment = null)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return Comment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @return string
     */
    public function getSalesforceOpportunityId()
    {
        return $this->salesforceOpportunityId;
    }

    /**
     * @param string $salesforceOpportunityId
     *
     * @return Order
     */
    public function setSalesforceOpportunityId($salesforceOpportunityId)
    {
        $this->salesforceOpportunityId = $salesforceOpportunityId;

        return $this;
    }

    /**
     * @return string
     */
    public function getSalesforceLeadId()
    {
        return $this->salesforceLeadId;
    }

    /**
     * @param string $salesforceLeadId
     *
     * @return Order
     */
    public function setSalesforceLeadId($salesforceLeadId)
    {
        $this->salesforceLeadId = $salesforceLeadId;

        return $this;
    }

    /**
     * Set salesForceAccountId
     *
     * @param string $salesForceAccountId
     *
     * @return Order
     */
    public function setSalesForceAccountId($salesForceAccountId)
    {
        $this->salesForceAccountId = $salesForceAccountId;

        return $this;
    }

    /**
     * Get salesForceAccountId
     *
     * @return string
     */
    public function getSalesForceAccountId()
    {
        return $this->salesForceAccountId;
    }

    /**
     * @return int
     */
    public function getTransportType()
    {
        return $this->transportType;
    }

    /**
     * @param int $transportType
     */
    public function setTransportType($transportType)
    {
        $this->transportType = $transportType;
    }

    /**
     * Set exported
     *
     * @param boolean $exported
     *
     * @return Order
     */
    public function setExported($exported)
    {
        $this->exported = $exported;

        return $this;
    }

    /**
     * Get exported
     *
     * @return boolean
     */
    public function getExported()
    {
        return $this->exported;
    }


    /**
     * Set exported
     *
     * @param boolean $exported
     *
     * @return Order
     */
    public function setHash($hash)
    {
        $this->hash = $hash;

        return $this;
    }

    /**
     * Get exported
     *
     * @return boolean
     */
    public function getHash()
    {
        return $this->hash;
    }

    public function getAddressStr()
    {
        $addresses = $this->getAddresses();

        if ($addresses) {
            $addressesForApi = [];

            /** @var Address[] $addresses */
            foreach ($addresses as $address) {
                $addressString = [];
                if ($address->getAddress()) {
                    $addressString[] = $address->getAddress();
                }
                if ($address->getAddress2()) {
                    $addressString[] = $address->getAddress2();
                }
                if ($address->getCity() || $address->getZip()) {
                    $addressString[] = $address->getCity() . ' ' . $address->getZip();
                }
                if ($address->getState()) {
                    $addressString[] = $address->getState();
                }
                if ($address->getCountry()) {
                    $addressString[] = $address->getCountry();
                }

                $addressesForApi[$address->getType()][$address->getPosition()] = implode(', ', $addressString);
            }

            $origins = rawurlencode(implode('|', $addressesForApi[0]));
            $destinations = rawurlencode(implode('|', $addressesForApi[1]));
            $units = 'metric';
            return self::GOOGLE_API_URL . 'origins=' . $origins
                   . '&destinations=' . $destinations . '&units=' . $units;
        }

        return [];
    }

    public function getOrderNumber()
    {
        return strrev($this->getLeadId());
    }

    public function getFullName()
    {
        return sprintf('%s %s', $this->getFirstname(), $this->getLastname());
    }

    /**
     * Properties collection helper for OrderAdmin
     *
     * @param array $field List of fields as array for multi field support (like volume and volume_unit in one query)
     * @return null|string
     */
    public function getPropertyDetail(array $field)
    {
        $result = null; // Default

        if ($field) {
            $properties = $this->getProperties();
            if ($properties) {
                $result = '';
                foreach ($field as $fieldName) {
                    foreach ($properties as $property2order) {
                        if (strtolower($fieldName) == strtolower($property2order->getProperty())) {
                            if (!empty($result)) {
                                $result .= ' '; // add a little space
                            }
                            $result .= $property2order->getValue();
                        }
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Address helper for OrderAdmin
     *
     * @param string $type Address type
     * @param string $column Address table field
     *
     * @return null|string|object
     */
    public function getAddressDetail($type = 'start', $column = '')
    {
        $address = $this->getAddresses();
        $addressType = (('start' == $type) ? '0' : '999');
        $result = null;

        if ($address && !empty($column)) {
            foreach ($address as $partAddress) {
                if ($addressType == $partAddress->getPosition()) {
                    switch ($column) {
                        case 'zip':
                            $result = $partAddress->getZip();
                            break;
                        case 'address':
                            $result = $partAddress->getAddress();
                            break;
                        case 'city':
                            $result = $partAddress->getCity();
                            break;
                        case 'country':
                            $result = $partAddress->getCountry();
                            break;
                        case 'address2':
                            $result = $partAddress->getAddress2();
                            break;
                        case 'region':
                            $result = $partAddress->getRegion();
                            break;
                        case 'state':
                            $result = $partAddress->getState();
                            break;
                        case 'countryiso2':
                            $result = $partAddress->getCountryIso2();
                            break;
                        case 'all':
                            $result = $partAddress;
                            break;
                        default:
                            $result = null;
                            break;
                    }
                }
            }
        }

        return $result;
    }

    public function getPrice()
    {
        if (null !== $this->getRealizedprice()) {
            return $this->getRealizedprice();
        }

        return $this->getInitialprice();
    }

    public function getGuid()
    {
        return $this->guid;
    }

    public function setGuid($value)
    {
        return $this->guid = $value;
    }

    /**
     * @param string $stageName
     */
    public function setStageName($stageName)
    {
        $this->stageName = $stageName;
    }

    /**
     * @return string
     */
    public function getStageName()
    {
        return $this->stageName;
    }

    /**
     * @return bool
     */
    public function getLocked()
    {
        return $this->locked;
    }

    /**
     * @param bool $locked
     * @return $this
     */
    public function setLocked($locked)
    {
        $this->locked = $locked;
    }

    /**
     * @param integer $billingAddressType
     */
    public function setBillingAddressType($billingAddressType)
    {
        $this->billingAddressType = $billingAddressType;
    }

    /**
     * @return integer
     */
    public function getBillingAddressType()
    {
        return $this->billingAddressType;
    }

    /**
     * Add insurance
     *
     * @param \LeadsBundle\Entity\Insurance $property
     *
     * @return Order
     */
    public function addInsurance(\LeadsBundle\Entity\Insurance $property)
    {
        $this->insurances[] = $property;

        return $this;
    }

    /**
     * Remove property
     *
     * @param Insurance $property
     */
    public function removeInsurance(\LeadsBundle\Entity\Insurance $property)
    {
        $this->insurances->removeElement($property);
    }

    /**
     * Get properties
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInsurance()
    {
        return $this->insurances;
    }
}