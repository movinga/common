<?php

namespace MovingaCommon\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Order
 *
 * @ORM\Table(name="`PortData`")
 * @ORM\Entity(repositoryClass="MovingaCommon\CommonBundle\Repository\PortDataRepository")
 */
class PortData implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * The Port name (departure)
     *
     * @var string
     *
     * @ORM\Column(name="port_from", type="string", length=255, nullable=false)
     */
    private $portFrom;

    /**
     * The Port name (arrive)
     *
     * @var string
     *
     * @ORM\Column(name="port_to", type="string", length=255, nullable=false)
     */
    private $portTo;

    /**
     * Route between the two route
     *
     * @ORM\Column(name="transport_route", type="text")
     */
    private $route;

    /**
     * @var float
     *
     * @ORM\Column(name="distance_km", type="float", nullable=true)
     */
    private $distanceInKm;

    /**
     * @var float
     *
     * @ORM\Column(name="distance_sea_miles", type="float", nullable=true)
     */
    private $distanceInSeaMiles;

    /**
     * @var float
     *
     * @ORM\Column(name="distance_miles", type="float", nullable=true)
     */
    private $distanceInMiles;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPortFrom()
    {
        return $this->portFrom;
    }

    /**
     * @param string $portFrom
     */
    public function setPortFrom($portFrom)
    {
        $this->portFrom = $portFrom;
    }

    /**
     * @return string
     */
    public function getPortTo()
    {
        return $this->portTo;
    }

    /**
     * @param string $portTo
     */
    public function setPortTo($portTo)
    {
        $this->portTo = $portTo;
    }

    /**
     * @return mixed
     */
    public function getRoute()
    {
        return $this->route;
    }

    /**
     * @param mixed $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }

    /**
     * @return float
     */
    public function getDistanceInKm()
    {
        return $this->distanceInKm;
    }

    /**
     * @param float $distanceInKm
     */
    public function setDistanceInKm($distanceInKm)
    {
        $this->distanceInKm = $distanceInKm;
    }

    /**
     * @return float
     */
    public function getDistanceInSeaMiles()
    {
        return $this->distanceInSeaMiles;
    }

    /**
     * @param float $distanceInSeaMiles
     */
    public function setDistanceInSeaMiles($distanceInSeaMiles)
    {
        $this->distanceInSeaMiles = $distanceInSeaMiles;
    }

    /**
     * @return float
     */
    public function getDistanceInMiles()
    {
        return $this->distanceInMiles;
    }

    /**
     * @param float $distanceInMiles
     */
    public function setDistanceInMiles($distanceInMiles)
    {
        $this->distanceInMiles = $distanceInMiles;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
}
