<?php

namespace MovingaCommon\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Order
 *
 * @ORM\Table(name="`PortDataCity`")
 * @ORM\Entity(repositoryClass="MovingaCommon\CommonBundle\Repository\PortDataCityRepository")
 */
class PortDataCity implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="port_name", type="string", length=255, nullable=false)
     */
    private $portName;

    /**
     * @var string
     *
     * @ORM\Column(name="port_city", type="string", length=255, nullable=false)
     */
    private $portCity;

    /**
     * The Port name (departure)
     *
     * @var string
     *
     * @ORM\Column(name="port_zip", type="string", length=8, nullable=true)
     */
    private $portZip;

    /**
     * @var string
     *
     * @ORM\Column(name="combined_name", type="string", length=255, nullable=true)
     */
    private $combinedName;

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getPortName()
    {
        return $this->portName;
    }

    /**
     * @param string $portName
     */
    public function setPortName($portName)
    {
        $this->portName = $portName;
    }

    /**
     * @return string
     */
    public function getPortCity()
    {
        return $this->portCity;
    }

    /**
     * @param string $portCity
     */
    public function setPortCity($portCity)
    {
        $this->portCity = $portCity;
    }

    /**
     * @return string
     */
    public function getPortZip()
    {
        return $this->portZip;
    }

    /**
     * @param string $portZip
     */
    public function setPortZip($portZip)
    {
        $this->portZip = $portZip;
    }

    /**
     * @return string
     */
    public function getCombinedName()
    {
        return $this->combinedName;
    }

    /**
     * @param string $combinedName
     */
    public function setCombinedName($combinedName)
    {
        $this->combinedName = $combinedName;
    }
}
