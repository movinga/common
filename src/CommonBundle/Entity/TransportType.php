<?php

namespace MovingaCommon\CommonBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Order
 *
 * @ORM\Table(
 *  name="`TransportType`"
 * )
 * @ORM\Entity
 */
class TransportType implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @ORM\OneToMany(targetEntity="Order", mappedBy="transportType")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="transport_type_name", type="string", length=255, nullable=false)
     */
    private $transportTypeName;

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTransportTypeName()
    {
        return $this->transportTypeName;
    }

    /**
     * @param string $transportTypeName
     */
    public function setTransportTypeName($transportTypeName)
    {
        $this->transportTypeName = $transportTypeName;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        $vars = get_object_vars($this);

        return $vars;
    }
}
