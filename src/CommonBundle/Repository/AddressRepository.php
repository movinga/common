<?php

namespace MovingaCommon\CommonBundle\Repository;

use Doctrine\ORM\EntityRepository;
use MovingaCommon\CommonBundle\Entity\Address;
use MovingaCommon\CommonBundle\Entity\Order;

/**
 * Custom repository for Address entity
 **/
class AddressRepository extends EntityRepository
{
    /**
     * @param Order $order
     * @param int   $type
     * @param int   $position
     *
     * @return Address
     */
    public function getAddressByTypeAndPosition(Order $order, $type, $position)
    {
        return $order->getAddresses()->filter(
            function (Address $address) use ($type, $position) {
                return $address->getType() == $type && $address->getPosition() == $position;
            }
        )->first();
    }

    /**
     * @param Order $order
     * @param int   $position
     *
     * @return Address
     */
    public function getAddressByPosition(Order $order, $position)
    {
        return $order->getAddresses()->filter(
            function (Address $address) use ($position) {
                return $address->getPosition() == $position;
            }
        )->first();
    }

    /**
     * @param Order $order
     *
     * @return null|Address
     */
    public function getLastAddress(Order $order)
    {
        return $order->getAddresses()->filter(
            function (Address $address) {
                return $address->getPosition() == Address::LASTADDRESSPOSITION;
            }
        )->first();
    }

    /**
     * @param Order $order
     *
     * @return null|Address
     */
    public function getFirstAddress(Order $order)
    {
        return $order->getAddresses()->filter(
            function (Address $address) {
                return $address->getPosition() == Address::FIRSTADDRESSPOSITION;
            }
        )->first();
    }
}
