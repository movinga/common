<?php

namespace MovingaCommon\CommonBundle\Repository;

use Doctrine\ORM\EntityRepository;
use MovingaCommon\CommonBundle\Entity\Order;

/**
 * Class OrderRepository
 *
 * @package LeadsBundle\Repository
 */
class OrderRepository extends EntityRepository
{
    /**
     * Finds Order by given salesforce Opportunity ID
     *
     * @param string $id
     *
     * @return null|Order
     */
    public function findBySalesforceOpportunityId($id)
    {
        return $this->findOneBy(['salesforceOpportunityId' => $id]);
    }

    /**
     * Finds Order by given salesforce LeadId
     *
     * @param string $id
     *
     * @return null|object
     */
    public function findByGuid($id)
    {
        return $this->findOneBy(['guid' => $id]);
    }


    /**
     * Finds Order by given salesforce LeadId
     *
     * @param string $id
     *
     * @return null|object
     */
    public function findBySalesforceLeadId($id)
    {
        return $this->findOneBy(['salesforceLeadId' => $id]);
    }

    /**
     * @return array
     */
    public function findNotExportedLeads()
    {
        $em = $this->getEntityManager();

        //create the query

        $query = $em->createQuery(
//            "SELECT o FROM CommonBundle:Order o WHERE o.exported = false AND ((o.providerId = 'immoScout') OR (o.providerId = 'skydreams' AND o.language = 'German') OR (o.country = 'United Kingdom' and (o.providerId != 'triglobal' AND o.providerId != 'pinlocal')))"
//            "SELECT o FROM CommonBundle:Order o WHERE o.exported = false AND ((o.providerId = 'immoScout') OR (o.providerId = 'umzugslupe') OR (o.providerId = 'Movinga.fr') OR (o.providerId = 'Movinga.it') OR (o.providerId = 'Movinga.co.uk') OR (o.providerId = 'skydreams' AND o.leadReceived > '2016-02-04 18:00:00' AND (o.language = 'de' OR o.language = 'German' OR o.language = 'en' OR o.language = 'English')))"
            "SELECT o FROM CommonBundle:Order o WHERE o.exported = false AND ((o.providerId = 'Immobiliare.it') OR (o.providerId = 'SalzBrot') OR (o.providerId = 'locaberlin') OR (o.providerId = 'aboalarm') OR (o.providerId = 'Wohnen im Alter') OR (o.providerId = 'meilleurtaux') OR (o.providerId = 'Wohnraumkarte.de') OR (o.providerId = 'Myhome.ie') OR (o.providerId = 'Sotravo') OR (o.providerId = 'Quoka.de') OR (o.providerId = 'Focus.de') OR (o.providerId = 'Mondevis') OR (o.providerId = 'conveychoiceuk') OR (o.providerId = 'Wikicasa') OR (o.providerId = 'Wickedin') OR (o.providerId = 'Adzuna') OR (o.providerId = 'hotraslocato') OR (o.providerId = 'lireco') OR (o.providerId = 'Trovacasa') OR (o.providerId = 'LeLynx.fr') OR ((o.providerId = 'generic') AND (o.leadReceived > '2016-06-06 00:00:00')) OR (o.providerId = 'eGentic') OR (o.providerId = 'Casa.it') OR (o.providerId = 'RWE') OR (o.providerId = 'leadsforlawyers') OR (o.providerId = 'Verivox') OR (o.providerId = 'Seloger.com') OR (o.providerId = 'Trovit') OR (o.providerId = 'Placebuzz') OR (o.providerId = 'Houser.co.uk') OR (o.providerId = 'immoScout') OR (o.providerId = 'Trovit') OR (o.providerId = 'Casa.it') OR (o.providerId = 'umzugslupe') OR (o.providerId = 'Movinga.fr') OR (o.providerId = 'Movinga.it') OR (o.providerId = 'Movinga.co.uk') OR (o.providerId = 'Movinga.es') OR (o.providerId = 'Moveria.de') OR (o.providerId = 'Moveria.fr') OR (o.providerId = 'Immoveo.de') OR (o.providerId = 'Housepals.co.uk') OR (o.providerId = 'Fubra.co.uk') OR (o.providerId = 'StoragePriceComparison.co.uk') OR (o.providerId = 'Adzuna.co.uk') OR (o.providerId = 'TheHouseShop.com') OR ((o.leadReceived > '2016-02-05 20:00:00') AND ((o.providerId = 'pinlocal') OR (o.providerId = 'movehub') OR (o.providerId = 'triglobal'))) OR (o.providerId = 'skydreams' AND o.leadReceived > '2016-02-04 18:00:00' ))");
        return $query->getResult();
    }

    /**
     * Only for providers to check if lead id exists (only returns leadId)
     *
     * @param $provider
     *
     * @return array
     */
    public function findByProvider($provider)
    {
        $em = $this->getEntityManager();
        $query = $em->createQuery(
            "SELECT o.leadId FROM CommonBundle:Order o WHERE o.providerId = :provider"
        );
        $query->setParameter(':provider', $provider);
        return $query->getResult();
    }

    public function IsGuidUnique($guid)
    {
        if ('' === $guid) {
            return false;
        }

        $em = $this->getEntityManager();
        $query = $em->createQuery(
            "SELECT COUNT(o) FROM CommonBundle:Order o WHERE o.guid = :guid"
        );
        $query->setParameter(':guid', $guid);
        return $query->getSingleScalarResult() == 0;
    }

    /**
     * @param $opportunityId
     * @return mixed
     */
    public function findByOpportunityId($opportunityId)
    {
        return $this->findOneBy(array('salesforceOpportunityId' => $opportunityId));
    }

    /**
     * @return mixed
     */
    public function findEmptyStageName()
    {
        $em = $this->getEntityManager();
        /** @var  Order $order */
        $orders = $em->createQuery('SELECT o FROM CommonBundle:Order o WHERE o.stageName IS NULL')->getResult();

        return $orders;
    }
}
