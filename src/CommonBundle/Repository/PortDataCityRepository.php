<?php

namespace MovingaCommon\CommonBundle\Repository;

use Doctrine\ORM\EntityRepository;
use MovingaCommon\CommonBundle\Entity\PortDataCity;

/**
 * Class PortDataCityRepository
 *
 * @package LeadsBundle\Repository
 */
class PortDataCityRepository extends EntityRepository
{
    /**
     * Finds PortCity by given ID
     *
     * @param int $id
     * @return null|PortData
     */
    public function findById($id = 0)
    {
        if (isset($id) && is_int($id) && 0 != $id) {
            return $this->findOneBy(['id' => $id]);
        }

        return null;
    }

    /**
     * Gives PortCity by Port name
     *
     * @param string $name
     * @return null|object
     */
    public function findByPortName($name = '')
    {
        if (isset($name) && '' != $name) {
            return $this->findOneBy(['port_name' => $name]);
        }

        return null;
    }

    /**
     * Gives PortCity by city name
     *
     * @param string $city
     * @return null|object
     */
    public function findByPortCity($city = '')
    {
        if (isset($city) && '' != $city) {
            return $this->findOneBy(['port_city' => $city]);
        }

        return null;
    }

    /**
     * Gives PortCity by zip code
     *
     * @param string $zip
     * @return null|object
     */
    public function findByZip($zip = '')
    {
        if (isset($zip) && '' != $zip) {
            return $this->findOneBy(['port_zip' => $zip]);
        }

        return null;
    }
}
