<?php

namespace MovingaCommon\CommonBundle\Repository;

use Doctrine\ORM\EntityRepository;
use MovingaCommon\CommonBundle\Entity\PortData;

/**
 * Class PortDataRepository
 *
 * @package LeadsBundle\Repository
 */
class PortDataRepository extends EntityRepository
{
    /**
     * Finds Port by given ID
     *
     * @param int $id
     * @return null|PortData
     */
    public function findById($id = 0)
    {
        if (isset($id) && is_int($id) && 0 != $id) {
            return $this->findOneBy(['id' => $id]);
        }

        return null;
    }

    /**
     * Port by name
     *
     * @param string $name
     * @return null|PortData
     */
    public function findByPortFrom($name)
    {
        if (isset($name) && '' != $name) {
            return $this->findOneBy(['port_from' => $name]);
        }

        return null;
    }

    /**
     * Port by name
     *
     * @param string $name
     * @return null|PortData
     */
    public function findByPortTo($name)
    {
        if (isset($name) && '' != $name) {
            return $this->findOneBy(['port_to' => $name]);
        }

        return null;
    }


    /**
     * Finds Port by distance in KM
     *
     * @param float $km
     * @return null|object
     */
    public function findByDistanceKm($km)
    {
        return $this->findOneBy(['distance_km']);
    }

    /**
     * Finds Port by distance in Miles
     *
     * @param float $miles
     * @return null|object
     */
    public function findByDistanceMiles($miles)
    {
        return $this->findOneBy(['distance_miles']);
    }

    /**
     * Finds Port by distance in Sea Miles
     *
     * @param float $seamiles
     * @return null|object
     */
    public function findByDistanceSeaMiles($seamiles)
    {
        return $this->findOneBy(['distance_sea_miles']);
    }
}
